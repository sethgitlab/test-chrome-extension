document.body.getElementsByClassName('board-extra-actions')[0].insertAdjacentHTML('beforebegin', '<div class="board-extra-actions"><a href="#" id="toggleLabels" data-qa-selector="label_button" role="button" aria-label="Hide Labels" title="" class="btn btn-default has-tooltip prepend-left-10 js-focus-mode-btn" data-original-title="Toggle labels">LB</a></div>');


document.getElementById("toggleLabels").addEventListener("click", toggleLabelsFunction);

var labelsShown = true;

function toggleLabelsFunction()
{
	if (labelsShown) {
		var styles = `
			button.color-label {display: none !important}
			span.color-label {display: none !important}
		    .board-card-labels {visibility: hidden}
		`

		var styleSheet = document.createElement("style")
		styleSheet.id = "glSS"
		styleSheet.type = "text/css"
		styleSheet.innerText = styles
		document.head.appendChild(styleSheet)

		labelsShown = false
	}
	else
	{
		var element = document.getElementById("glSS");
		element.parentNode.removeChild(element);
		labelsShown = true;
	}

	// var elements. = document.getElementsByClassName('board-card-labels');
	// for (var i = 0; i < elements.length; i++) {
	//     elements[i].innerHTML="";
	// }
}

