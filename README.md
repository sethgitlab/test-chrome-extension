# Test Chrome Extension
A sample project for hacking together some GitLab.com features via a chrome extension. 

To load this extension, go to chrome://extensions/, turn on Developer Mode, and then click "Load Unpacked" and select the folder containing the repo files. 

Feature:
* Hide labels on issue boards ![Issue Board](images/hide-labels.png)